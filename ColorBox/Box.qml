import QtQuick 2.0

Item {
    id: root
    property alias text: label.text
    property alias color: rect.color

    signal clicked
    signal exited
    signal entered
    signal wheel

    Rectangle {
        id: rect
        width: 300
        height: 300
        anchors.centerIn: parent
        border.color: "#000"


        Text {
            id: label
            height: parent.height
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            color: "#fff"
            font.pixelSize: 18
            elide: Text.ElideRight

        }

        MouseArea {
            anchors.fill: parent
            onClicked: root.clicked()
            onWheel: root.wheel()
            onEntered: root.entered()
            onExited: root.exited()
        }
    }



}
