import QtQuick 2.0


FocusScope {
    id:  root
    width: parent.width
    height: 30

    property string color
    property alias text: input.text
    signal accepted(string text)

    Flow {
        anchors.fill: parent
        anchors.margins: 4

        Text {
            id: label
            text: 'Color:'
            width: 50
            height: parent.height
            verticalAlignment: Text.AlignVCenter
        }

        TextInput {
            id: input
            width: parent.width - 50
            height: parent.height
            anchors.margins: 4
            focus: true
            font.pixelSize: 18
            verticalAlignment: Text.AlignVCenter
            onAccepted: root.accepted(text)
        }
    }
}
