import QtQuick 2.0
//import components.atom 1.0 as Comp


Comp.Box {
    id: root
    property string newColor: color

    onClicked: clickedAnimation.start()
    onWheel: console.log("onWheel")


    SequentialAnimation {
        id: clickedAnimation

        PropertyAction {
            target: root
            property: "color"
            value: "white"
        }
        ColorAnimation {
            target: root
            property: "color"
            to: newColor
            duration: 1000
        }
    }

}
