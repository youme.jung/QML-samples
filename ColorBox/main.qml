import QtQuick 2.12
import QtQuick.Window 2.12
//import components.atom 1.2

Window {
    id: windowRoot
    width: 500
    height: 500
    visible: true
    title: qsTr("ColorBox")

    Flow {
        flow: Flow.TopToBottom
        anchors.fill: parent

        ColorInput {
            id: colorInput
            focus: true
            onAccepted: {
                print('input message:' + text)
                color = text
                colorInput.text = ''
            }
        }

        Box {
            id: myColorBox
            width: parent.width
            height: parent.height - colorInput.height
            color: colorInput.color
            text: colorInput.color


        }
    }

}
